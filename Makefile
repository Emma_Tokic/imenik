CC=gcc
CFLAGS=-g -Wall

todo: main.o kontakt.o
	$(CC) main.o kontakt.o -o kontakt.exe

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

kontakt.o: kontakt.c
	$(CC) $(CFLAGS) -c kontakt.c

clear:
	$(RM) *.exe *.o