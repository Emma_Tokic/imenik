#include <stdio.h>

#include "kontakt.h"

const int maks_kontakt = 1;

int main() {
  kontakt kontakti[maks_kontakt];

  for (int i = 0; i < maks_kontakt; i++) {
    create_kontakt(&kontakti[i]);
  }

  for (int i = 0; i < maks_kontakt; i++) {
    printf("-------------------------------\n");
    printf("Name : %s\n", kontakti[i].ime);
    printf("Surname : %s\n", kontakti[i].prezime);
    printf("Phone : %d\n", kontakti[i].broj_telefona);
  }

  return 1;
}