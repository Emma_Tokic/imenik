#include "kontakt.h"

#include <stdio.h>

void create_kontakt(kontakt* k) {
  printf("++++++++++++++++++++++++++++\n");

  printf("Enter name :\n");
  scanf("%s", (*k).ime);
  getchar();

  printf("Enter surname :\n");
  scanf("%s", k->prezime);
  getchar();

  printf("Enter phone number :\n");
  scanf("%d", &(*k).broj_telefona);
  getchar();
}